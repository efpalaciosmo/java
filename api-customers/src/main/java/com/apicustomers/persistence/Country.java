package com.apicustomers.persistence;

import jakarta.persistence.*;

@Entity(name = "country")
@Table(name = "country")
public class Country {
    @Id
    @SequenceGenerator(
            name = "country_id_sequence",
            sequenceName = "country_id_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            generator = "country_id_sequence",
            strategy = GenerationType.SEQUENCE
    )
    @Column(
            name = "country_id",
            updatable = false
    )
    private Long country_id;
    @Column(
            name = "country_name",
            nullable = false,
            columnDefinition = "VARCHAR(40)"
    )
    private String country_name;
    @Column(
            name = "region_id",
            nullable = false,
            columnDefinition = "INTEGER"
    )
    private Integer region_id;

    public Country() {
    }
    public Country(String country_name, Integer region_id) {
        this.country_name = country_name;
        this.region_id = region_id;
    }
    public Country(Long country_id, String country_name, Integer region_id) {
        this.country_id = country_id;
        this.country_name = country_name;
        this.region_id = region_id;
    }
    public Long getCountry_id() {
        return country_id;
    }
    public void setCountry_id(Long country_id) {
        this.country_id = country_id;
    }
    public String getCountry_name() {
        return country_name;
    }
    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }
    public Integer getRegion_id() {
        return region_id;
    }
    public void setRegion_id(Integer region_id) {
        this.region_id = region_id;
    }
}