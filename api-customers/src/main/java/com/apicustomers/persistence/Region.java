package com.apicustomers.persistence;

import jakarta.persistence.*;
@Entity(name = "region")
@Table(name = "region")
public class Region {
    @Id
    @SequenceGenerator(
            name = "region_id_sequence",
            sequenceName = "region_id_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            generator = "region_id_sequence",
            strategy = GenerationType.SEQUENCE
    )
    @Column(
            name = "location_id",
            updatable = false
    )
    private Long region_id;
    @Column(
            name = "region_name",
            columnDefinition = "VARCHAR(25)"
    )
    private String region_name;

    public Region() {
    }

    public Region(String region_name) {
        this.region_name = region_name;
    }

    public Region(Long region_id, String region_name) {
        this.region_id = region_id;
        this.region_name = region_name;
    }

    public Long getRegion_id() {
        return region_id;
    }

    public void setRegion_id(Long region_id) {
        this.region_id = region_id;
    }

    public String getRegion_name() {
        return region_name;
    }
    public void setRegion_name(String region_name) {
        this.region_name = region_name;
    }

    @Override
    public String toString() {
        return "Region{" +
                "region_id=" + region_id +
                ", region_name='" + region_name + '\'' +
                '}';
    }
}
