package com.apicustomers.persistence;

import jakarta.persistence.*;
@Entity(name = "location")
@Table(name = "location")
public class Location {
    @Id
    @SequenceGenerator(
            name = "location_id_sequence",
            sequenceName = "location_id_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            generator = "location_id_sequence",
            strategy = GenerationType.SEQUENCE
    )
    @Column(
            name = "location_id",
            updatable = false
    )
    private Long location_id;
    @Column(
            name = "street_address",
            columnDefinition = "VARCHAR(40)"
    )
    private String street_address;
    @Column(
            name = "postal_code",
            columnDefinition = "VARCHAR(20)"
    )
    private String postal_code;
    @Column(
            name = "city",
            nullable = false,
            columnDefinition = "VARCHAR(30)"
    )
    private String city;
    @Column(
            name = "state_province",
            columnDefinition = "VARCHAR(25)"
    )
    private String state_province;
    @Column(
            name = "country_id",
            columnDefinition = "VARCHAR(2)"
    )
    private Long country_id;

    public Location() {
    }

    public Location(Long location_id, String city) {
        this.location_id = location_id;
        this.city = city;
    }

    public Location(Long location_id, String street_address, String postal_code, String city, String state_province, Long country_id) {
        this.location_id = location_id;
        this.street_address = street_address;
        this.postal_code = postal_code;
        this.city = city;
        this.state_province = state_province;
        this.country_id = country_id;
    }

    public Long getLocation_id() {
        return location_id;
    }

    public void setLocation_id(Long location_id) {
        this.location_id = location_id;
    }

    public String getStreet_address() {
        return street_address;
    }

    public void setStreet_address(String street_address) {
        this.street_address = street_address;
    }

    public String getPostal_code() {
        return postal_code;
    }

    public void setPostal_code(String postal_code) {
        this.postal_code = postal_code;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState_province() {
        return state_province;
    }

    public void setState_province(String state_province) {
        this.state_province = state_province;
    }

    public Long getCountry_id() {
        return country_id;
    }

    public void setCountry_id(Long country_id) {
        this.country_id = country_id;
    }

    @Override
    public String toString() {
        return "Location{" +
                "location_id=" + location_id +
                ", street_address='" + street_address + '\'' +
                ", postal_code='" + postal_code + '\'' +
                ", city='" + city + '\'' +
                ", state_province='" + state_province + '\'' +
                ", country_id=" + country_id +
                '}';
    }
}
