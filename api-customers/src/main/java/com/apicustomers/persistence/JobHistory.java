package com.apicustomers.persistence;

import jakarta.persistence.*;

import java.time.LocalDateTime;

@Entity(name = "jobHistory")
@Table(name = "jobHistory")
public class JobHistory {
    @Id
    @SequenceGenerator(
            name = "job_history_id_sequence",
            sequenceName = "job_history_id_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            generator = "job_history_id_sequence",
            strategy = GenerationType.SEQUENCE
    )
    @Column(
            name = "employee_id",
            updatable = false
    )
    private Long employee_id;
    @Column(
            name = "start_date",
            nullable = false,
            columnDefinition = "TIMESTAMP WITHOUT TIME ZONE"
    )
    private LocalDateTime start_date;
    @Column(
            name = "end_date",
            nullable = false,
            columnDefinition = "TIMESTAMP WITHOUT TIME ZONE"
    )
    private LocalDateTime end_date;
    @Column(
            name = "job_id",
            updatable = false
    )
    private Long job_id;
    @Column(
            name = "department_id",
            updatable = false
    )
    private Long department_id;

    public JobHistory() {
    }

    public JobHistory(Long employee_id, LocalDateTime start_date, LocalDateTime end_date, Long job_id) {
        this.employee_id = employee_id;
        this.start_date = start_date;
        this.end_date = end_date;
        this.job_id = job_id;
    }

    public JobHistory(Long employee_id, LocalDateTime start_date, LocalDateTime end_date, Long job_id, Long department_id) {
        this.employee_id = employee_id;
        this.start_date = start_date;
        this.end_date = end_date;
        this.job_id = job_id;
        this.department_id = department_id;
    }

    public Long getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(Long employee_id) {
        this.employee_id = employee_id;
    }

    public LocalDateTime getStart_date() {
        return start_date;
    }

    public void setStart_date(LocalDateTime start_date) {
        this.start_date = start_date;
    }

    public LocalDateTime getEnd_date() {
        return end_date;
    }

    public void setEnd_date(LocalDateTime end_date) {
        this.end_date = end_date;
    }

    public Long getJob_id() {
        return job_id;
    }

    public void setJob_id(Long job_id) {
        this.job_id = job_id;
    }

    public Long getDepartment_id() {
        return department_id;
    }

    public void setDepartment_id(Long department_id) {
        this.department_id = department_id;
    }

    @Override
    public String toString() {
        return "JobHistory{" +
                "employee_id=" + employee_id +
                ", start_date=" + start_date +
                ", end_date=" + end_date +
                ", job_id=" + job_id +
                ", department_id=" + department_id +
                '}';
    }
}
