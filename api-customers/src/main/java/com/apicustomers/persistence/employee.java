package com.apicustomers.persistence;

import jakarta.persistence.*;

import java.time.LocalDateTime;
@Entity(name = "employee")
@Table(name = "employee")
public class employee {
    @Id
    @SequenceGenerator(
            name = "employee_id_sequence",
            sequenceName = "employee_id_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            generator = "employee_id_sequence",
            strategy = GenerationType.SEQUENCE
    )
    @Column(name = "employee_id")
    private Number employee_id;
    @Column(
            name = "first_name",
            columnDefinition = "VARCHAR(20)"
    )
    private String first_name;
    @Column(
            name = "last_name",
            nullable = false,
            columnDefinition = "VARCHAR(25)"
    )
    private String last_name;
    @Column(
            name = "email",
            nullable = false,
            columnDefinition = "VARCHAR(20)"
    )
    private String email;
    @Column(
            name = "phone_number",
            columnDefinition = "VARCHAR(20)"
    )
    private String phone_number;
    @Column(
            name = "hire_date",
            nullable = false,
            columnDefinition = "TIMESTAMP WITHOUT TIME ZONE"
    )
    private LocalDateTime hire_date;
    @Column(
            name = "job_id",
            nullable = false,
            columnDefinition = "VARCHAR(10)"
    )
    private String job_id;
    @Column(
            name = "salary",
            columnDefinition = "INTEGER"
    )
    private Integer salary;
    @Column(
            name = "commission_pct",
            columnDefinition = "INTEGER"
    )
    private Number commission_pct;
    @Column(
            name = "manager_id",
            columnDefinition = "INTEGER"
    )
    private Number manager_id;
    @Column(
            name = "department_id",
            columnDefinition = "INTEGER"
    )
    private Number department_id;

    public employee() {
    }

    public employee(String last_name, String email, LocalDateTime hire_date, String job_id) {
        this.last_name = last_name;
        this.email = email;
        this.hire_date = hire_date;
        this.job_id = job_id;
    }

    public employee(Number employee_id, String first_name, String last_name, String email, String phone_number, LocalDateTime hire_date, String job_id, Integer salary, Number commission_pct, Number manager_id, Number department_id) {
        this.employee_id = employee_id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        this.phone_number = phone_number;
        this.hire_date = hire_date;
        this.job_id = job_id;
        this.salary = salary;
        this.commission_pct = commission_pct;
        this.manager_id = manager_id;
        this.department_id = department_id;
    }

    public Number getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(Number employee_id) {
        this.employee_id = employee_id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public LocalDateTime getHire_date() {
        return hire_date;
    }

    public void setHire_date(LocalDateTime hire_date) {
        this.hire_date = hire_date;
    }

    public String getJob_id() {
        return job_id;
    }

    public void setJob_id(String job_id) {
        this.job_id = job_id;
    }

    public Integer getSalary() {
        return salary;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }

    public Number getCommission_pct() {
        return commission_pct;
    }

    public void setCommission_pct(Number commission_pct) {
        this.commission_pct = commission_pct;
    }

    public Number getManager_id() {
        return manager_id;
    }

    public void setManager_id(Number manager_id) {
        this.manager_id = manager_id;
    }

    public Number getDepartment_id() {
        return department_id;
    }

    public void setDepartment_id(Number department_id) {
        this.department_id = department_id;
    }

    @Override
    public String toString() {
        return "employee{" +
                "employee_id=" + employee_id +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", email='" + email + '\'' +
                ", phone_number='" + phone_number + '\'' +
                ", hire_date=" + hire_date +
                ", job_id='" + job_id + '\'' +
                ", salary=" + salary +
                ", commission_pct=" + commission_pct +
                ", manager_id=" + manager_id +
                ", department_id=" + department_id +
                '}';
    }
}
