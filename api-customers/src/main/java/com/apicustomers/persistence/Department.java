package com.apicustomers.persistence;

import jakarta.persistence.*;
@Entity(name = "department")
@Table(name = "department")
public class Department {
    @Id
    @SequenceGenerator(
            name = "department_id_sequence",
            sequenceName = "department_id_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            generator = "department_id_sequence",
            strategy = GenerationType.SEQUENCE
    )
    @Column(
            name = "department_id",
            updatable = false
    )
    private String department_id;
    @Column(
            name = "department_name",
            nullable = false,
            columnDefinition = "VARCHAR(30)"
    )
    private String department_name;
    @Column(
            name = "manager_id",
            nullable = false,
            columnDefinition = "INTEGER"
    )
    private Number manager_id;
    @Column(
            name = "location_id",
            nullable = false,
            columnDefinition = "INTEGER"
    )
    private Number location_id;

    public Department() {
    }

    public Department(String department_name, Number manager_id, Number location_id) {
        this.department_name = department_name;
        this.manager_id = manager_id;
        this.location_id = location_id;
    }

    public String getDepartment_id() {
        return department_id;
    }

    public void setDepartment_id(String department_id) {
        this.department_id = department_id;
    }

    public String getDepartment_name() {
        return department_name;
    }

    public void setDepartment_name(String department_name) {
        this.department_name = department_name;
    }

    public Number getManager_id() {
        return manager_id;
    }

    public void setManager_id(Number manager_id) {
        this.manager_id = manager_id;
    }

    public Number getLocation_id() {
        return location_id;
    }

    public void setLocation_id(Number location_id) {
        this.location_id = location_id;
    }

    @Override
    public String toString() {
        return "Department{" +
                "department_id='" + department_id + '\'' +
                ", department_name='" + department_name + '\'' +
                ", manager_id=" + manager_id +
                ", location_id=" + location_id +
                '}';
    }
}
