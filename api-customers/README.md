## Instructions

In order to run this project you need to have installed docker or podman

- To start the postgresql database run:
```shell
podman run \
      --name spring_jpa \
      -p 5432:5432 \
      -e POSTGRES_USER=root \
      -e POSTGRES_PASSWORD=root \
      -e POSTGRES_DB=customers \
      -d postgres:14.6
```

- Add the following dependencies to the pom.xml <br> 
`spring-boot-starter-data-jpa` <br>
`postgresql`<br>
`springdoc-openapi-starter-webmvc-ui`<br>

- To see the documentation for the API, go to:
```bash
http://localhost:8080/swagger-ui/index.html
```