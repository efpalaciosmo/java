# spring-data-jpa-course

## Project Description

In this project is everything about Spring Data JPA allowing to build scalable backend applications backed by any relational database. Spring Data JPA is a great choice allowing to speed your development and focus on the business logic. There will be a lot of coding In this course you will learn the following:

- What is Spring Data JPA
- Connect to a real database and not in memory DB
- How to map classes to tables
- Hibernate Entity Life Cycle
- Queries
- Paging and Sorting
- 1 to 1 Relationships
- 1 to Many Relationships
- Many to Many relationships
- Transactions

## Run postgres database
```shell
podman run \
      --name spring_jpa \
      -p 5432:5432 \
      -e POSTGRES_USER=root \
      -e POSTGRES_PASSWORD=root \
      -e POSTGRES_DB=spring_jpa \
      -d postgres:14.6
```
