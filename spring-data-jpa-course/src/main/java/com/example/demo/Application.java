package com.example.demo;

import com.github.javafaker.Faker;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.time.LocalDateTime;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    CommandLineRunner commandLineRunner(
            StudentRepository studentRepository,
            StudentIdCardRepository studentIdCardRepository,
            BookRepository bookRepository){
        return args -> {
            //generateRandomStudents(studentRepository);
            Student student = generateOneStudent(studentRepository);

            student.addBook(new Book(
                    "new book",
                    LocalDateTime.now().minusDays(4)
            ));

            StudentIdCard studentIdCard = new StudentIdCard(
                    student,
                    "123456789"
            );

            student.addEnrolment(new Enrolment(
                    new EnrolmentId(1L, 1L),
                    student,
                    new Course("Computer Science", "IT"),
                    LocalDateTime.now()
            ));

            studentRepository.save(student);

            studentRepository.findById(1L)
                            .ifPresent(System.out::println);

            studentIdCardRepository
                    .findById(1L)
                    .ifPresent(System.out::println);

            bookRepository
                    .findById(1L)
                    .ifPresent(System.out::println);
        };
    }

    // To create pagination, on this case studentRepository implements PagingAndSortingRepository
    private Page<Student> pagination(StudentRepository studentRepository) {
        PageRequest pageRequest = PageRequest.of(
                0,
                50,
                Sort.by("firstName").descending());
        Page<Student> page = studentRepository.findAll(pageRequest);
        return page;
    }

    // how to sort queries, on this case StudentRepository implements JPA...
    private void sorting(StudentRepository studentRepository) {
        Sort sort = Sort.by("firstName")
                .descending()
                .and(Sort.by("age").descending());
        studentRepository.findAll(sort)
                .forEach(student -> System.out.println(student.getFirstName() + " " + student.getAge()));
    }

    private void generateRandomStudents(StudentRepository studentRepository) {
        Faker faker = new Faker();
        for (int i = 0; i < 10; i++) {
            String firstName = faker.name().firstName();
            String lastName = faker.name().lastName();
            String email = String.format("%s.%s@unal.edu.co", firstName, lastName);
            Student student = new Student(
                    firstName,
                    lastName,
                    email,
                    faker.number().numberBetween(18, 80)
            );
            studentRepository.save(student);
        }
    }
    private Student generateOneStudent(StudentRepository studentRepository){
        Faker faker = new Faker();
        String firstName = faker.name().firstName();
        String lastName = faker.name().lastName();
        String email = String.format("%s.%s@unal.edu.co", firstName, lastName);
        Student student = new Student(
                firstName,
                lastName,
                email,
                faker.number().numberBetween(18, 80)
        );
        return student;
    }
}
