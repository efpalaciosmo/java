package com.example.demo;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@Entity(name = "Book")
@Table(name = "book")
public class Book {

    @Id
    @SequenceGenerator(
            name = "generate_sequence_book_id",
            sequenceName = "generate_sequence_book_id",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "generate_sequence_book_id"
    )
    @Column(
            name = "id",
            updatable = false

    )
    private Long id;
    @ManyToOne
    @JoinColumn(
            name = "student_id",
            nullable = false,
            referencedColumnName = "id",
            foreignKey = @ForeignKey(
                    name = "student_id_book_fk"
            )
    )
    private Student student;


    @Column(
            name = "book_name",
            columnDefinition = "TEXT",
            nullable = false
    )
    private String bookName;
    @Column(
            name = "create_at",
            nullable = false,
            columnDefinition = "TIMESTAMP WITHOUT TIME ZONE"
    )
    private LocalDateTime createAt;



    public String getBook_name() {
        return bookName;
    }

    public void setBook_name(String book_name) {
        this.bookName = book_name;
    }
    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", student=" + student +
                ", book_name='" + bookName + '\'' +
                ", create_at=" + createAt +
                '}';
    }

    public Book(Long id, Student student, String book_name, LocalDateTime create_at) {
        this.id = id;
        this.student = student;
        this.bookName = book_name;
        this.createAt = create_at;
    }

    public Book(String book_name, LocalDateTime create_at) {
        this.bookName = book_name;
        this.createAt = create_at;
    }

    public Book() {

    }
}
