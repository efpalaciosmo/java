package com.example.demo;

import javax.persistence.*;

@Entity(name = "studentIdCard")
@Table(
        name = "studentIdCard",
        uniqueConstraints = {
                @UniqueConstraint(name = "student_id_card_number_unique", columnNames = "card_number")
        }
)
public class StudentIdCard {

    @Id
    @SequenceGenerator(
            name = "student_card_id_sequence",
            sequenceName = "student_card_id_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "student_card_id_sequence"
    )
    @Column(
            name = "id",
            updatable = false
    )
    private Long id;

    // To create the fk
    @OneToOne(
            cascade = CascadeType.ALL
    )
    @JoinColumn(
            name = "student_id",
            referencedColumnName = "id",
            foreignKey = @ForeignKey(
                    name = "student_id_fk"
            )
    )
    private Student student;

    @Column(
            name = "card_number",
            nullable = false,
            length = 15
    )
    private String card_number;

    public StudentIdCard(Student student, String card_number) {
        this.student = student;
        this.card_number = card_number;
    }

    public StudentIdCard(String card_number) {
        this.card_number = card_number;
    }

    public StudentIdCard() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCard_number() {
        return card_number;
    }

    public void setCard_number(String card_number) {
        this.card_number = card_number;
    }

    @Override
    public String toString() {
        return "studentIdCard{" +
                "id=" + id +
                ", student=" + student +
                ", card_number='" + card_number + '\'' +
                '}';
    }
}
