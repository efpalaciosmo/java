package com.example.demo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

// works without the @query annotation
public interface StudentRepository extends PagingAndSortingRepository<Student, Long> {
    @Query(value = "SELECT * FROM Student s WHERE s.email= :email", nativeQuery = true)
    Optional<Student> findStudentByEmail(@Param("email") String email);
    @Query("SELECT s FROM Student s WHERE s.firstName=?1 AND s.age=?2")
    List<Student> findStudentsByFirstNameEqualsAndAgeEquals(String firstName, Integer age);

    @Transactional
    @Modifying
    @Query("DELETE FROM Student s WHere s.id = :id")
    int deleteStudentById(@Param("id") Long id);
}
