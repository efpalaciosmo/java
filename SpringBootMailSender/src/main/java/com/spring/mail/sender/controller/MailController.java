package com.spring.mail.sender.controller;

import com.spring.mail.sender.DTOs.EmailDto;
import com.spring.mail.sender.DTOs.EmailFileDto;
import com.spring.mail.sender.services.IEmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.io.File;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/v1")
public class MailController {
    @Autowired
    private IEmailService emailService;
    @PostMapping("/sendMessage")
    public ResponseEntity<?> requestEmail(@RequestBody EmailDto emailDto){
        System.out.println("Message received " + emailDto);
        emailService.sendEmail(emailDto.getToUser(), emailDto.getSubject(), emailDto.getMessage());
        Map<String, String> response = new HashMap<>();
        response.put("Status", "Send");
        return ResponseEntity.ok(response);
    }

    public ResponseEntity<?> requestEmailWithFile(@ModelAttribute EmailFileDto emailFileDto){
        try{
            String filename = emailFileDto.getFile().getOriginalFilename();
            Path path = Paths.get("src/mail/resources/files/" + filename);
            try {
                Files.createDirectories(path.getParent());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            try {
                Files.copy(emailFileDto.getFile().getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            File file = path.toFile();
            emailService.sendEmailWithFile(emailFileDto.getToUser(), emailFileDto.getSubject(), emailFileDto.getMessage(), file);
            Map<String, String> response = new HashMap<>();
            response.put("Status", "Send");
            response.put("File", filename);
            return ResponseEntity.ok(response);
        } catch (RuntimeException e) {
            throw new RuntimeException("Error sending email: " + e);
        }
    }
}