package com.unal.hospital.mapper;

public interface IMapper<Input, Output> {
    public Output map(Input in);
}
