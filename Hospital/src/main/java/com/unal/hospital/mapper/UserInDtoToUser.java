package com.unal.hospital.mapper;

import com.unal.hospital.persistence.entity.User;
import com.unal.hospital.persistence.entity.dtos.UserDto;
import org.springframework.stereotype.Component;

@Component
public class UserInDtoToUser implements IMapper<UserDto, User> {
    @Override
    public User map(UserDto in) {
        User user = new User(
                in.getFirstName(),
                in.getLastName(),
                in.getEmail(),
                in.getPassword(),
                in.getPhone()
        );
        return user;
    }
}
