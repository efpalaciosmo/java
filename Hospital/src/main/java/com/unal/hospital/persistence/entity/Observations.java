package com.unal.hospital.persistence.entity;
import jakarta.persistence.*;
import lombok.Getter;

@Entity
@Getter
public class Observations {
    @Id
    @SequenceGenerator(name = "user_sequence", sequenceName = "user_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "student_sequence")
    private Long id;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(
            name = "user_id",
            nullable = false,
            referencedColumnName = "id",
            foreignKey = @ForeignKey(
                    name = "user_id_observation_fk"
            )
    )
    private User user;
    private String observation;
}
