package com.unal.hospital.persistence.entity.dtos;

import com.unal.hospital.persistence.entity.UserTypeEnum;
import lombok.Getter;

@Getter
public class UserDto {
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String phone;
    private UserTypeEnum type;
}