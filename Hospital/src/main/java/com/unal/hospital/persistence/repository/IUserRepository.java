package com.unal.hospital.persistence.repository;

import com.unal.hospital.persistence.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface IUserRepository extends JpaRepository<User, Long> {
    public Optional<User> findUserByEmail(String email);
}