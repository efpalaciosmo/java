package com.unal.hospital.exceptions;

import lombok.Getter;
import org.springframework.http.HttpStatus;
@Getter
public class UserExceptions extends RuntimeException{
    private String message;
    private HttpStatus httpStatus;

    public UserExceptions(String message, HttpStatus httpStatus){
        super(message);
        this.message = message;
        this.httpStatus = httpStatus;
    }
}