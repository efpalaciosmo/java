package com.unal.hospital.service;

import com.unal.hospital.exceptions.UserExceptions;
import com.unal.hospital.mapper.UserInDtoToUser;
import com.unal.hospital.persistence.entity.User;
import com.unal.hospital.persistence.entity.dtos.UserDto;
import com.unal.hospital.persistence.repository.IUserRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {
    private final UserInDtoToUser mapper;
    private final IUserRepository repository;

    public UserService(UserInDtoToUser mapper, IUserRepository repository) {
        this.mapper = mapper;
        this.repository = repository;
    }

    public User registerUser(UserDto userDto){
        String email = userDto.getEmail();
        Optional<User> user_db = this.repository.findUserByEmail(email);
        if(user_db.isPresent()){
            throw new UserExceptions("User already exist", HttpStatus.BAD_REQUEST);
        }
        User user = mapper.map(userDto);
        return this.repository.save(user);
    }
}