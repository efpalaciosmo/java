package org.example;

public class palindrome {
    public boolean isPalindrome(long x){
        String s = Long.toString(x);
        Boolean answer = true;
        if (x<0) {
            return false;
        }
        for (int i = 0; i < s.length()/2; i++) {
            if (s.charAt(i) != s.charAt(s.length() - 1 - i)) {
                answer = false;
                break;
            }
        }
        return answer;
    }
}
