package com.todoApp.exceptions;

import lombok.Getter;
import org.springframework.http.HttpStatus;
@Getter
public class ToDoExceptions extends RuntimeException{
    private String message;
    private HttpStatus httpStatus;

    public ToDoExceptions(String message, HttpStatus httpStatus){
        super(message);
        this.message = message;
        this.httpStatus = httpStatus;
    }
}