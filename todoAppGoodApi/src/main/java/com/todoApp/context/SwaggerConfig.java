package com.todoApp.context;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@Configuration  // means that this class has the ability to create beans
@EnableSwagger2
public class SwaggerConfig { // Class to define config swagger
    @Bean
    public Docket api(){
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.todoApp.controllers"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo());
    }
    private ApiInfo apiInfo(){
        return new ApiInfo(
                "Todo API",
                "La API REST de ToDo App.",
                "v1",
                "Term of service",
                new Contact(
                        "Efran Palacios",
                        "www.todoapi.com",
                        "efpalaciosmo@unal.edu.co"),
                        "License of API",
                        "API license URL",
                        Collections.emptyList());
    }
}