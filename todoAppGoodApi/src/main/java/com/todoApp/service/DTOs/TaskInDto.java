package com.todoApp.service.DTOs;

import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor
public class TaskInDto {
    private String title;
    private String description;
    private LocalDateTime eta; // finished estimated date
}
