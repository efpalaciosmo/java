package com.todoApp.service;

import com.todoApp.exceptions.ToDoExceptions;
import com.todoApp.mapper.TaskInDtoToTask;
import com.todoApp.persistence.entity.Task;
import com.todoApp.persistence.entity.TaskStatusEnum;
import com.todoApp.persistence.repository.ITaskRepository;
import com.todoApp.service.DTOs.TaskInDto;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class TaskService {
    private final TaskInDtoToTask mapper;
    private final ITaskRepository taskRepository;

    public TaskService(TaskInDtoToTask mapper, ITaskRepository taskRepository) {
        this.mapper = mapper;
        this.taskRepository = taskRepository;
    }

    public Task createTask(TaskInDto taskInDto){
        Task task = mapper.map(taskInDto);
        return this.taskRepository.save(task);
    }

    public List<Task> getAllTasks(){
        return this.taskRepository.findAll();
    }

    public List<Task> getAllTaskByTaskStatus(TaskStatusEnum statusEnum){
        return this.taskRepository.findAllByTaskStatus(statusEnum);
    }

    @Transactional // to performance an update
    public void markTaskAsFinished(Long id){
        Optional<Task> task = this.taskRepository.findById(id);
        if(task.isEmpty()){
            throw new ToDoExceptions("Task not found", HttpStatus.NOT_FOUND);
        }
        this.taskRepository.markTaskAsFinished(id);
    }

    public void deleteById(Long id){
        Optional<Task> task = this.taskRepository.findById(id);
        if(task.isEmpty()){
            throw new ToDoExceptions("Task not found", HttpStatus.NOT_FOUND);
        }
        this.taskRepository.deleteById(id);
    }
}