package com.todoApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TodoAppGoodApiApplication {
    public static void main(String[] args) {
        SpringApplication.run(TodoAppGoodApiApplication.class, args);
    }
}