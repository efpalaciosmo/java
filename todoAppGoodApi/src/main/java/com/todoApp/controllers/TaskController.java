package com.todoApp.controllers;

import com.todoApp.persistence.entity.Task;
import com.todoApp.persistence.entity.TaskStatusEnum;
import com.todoApp.service.DTOs.TaskInDto;
import com.todoApp.service.TaskService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/tasks")
public class TaskController {
    private final TaskService taskService;

    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }
    @PostMapping
    public Task createTask(@RequestBody TaskInDto taskInDto){
        return this.taskService.createTask(taskInDto);
    }
    @GetMapping
    public List<Task> getAllTasks(){
        return this.taskService.getAllTasks();
    }

    @GetMapping("/status/{status}")
    public List<Task> getAllTaskByStatus(@PathVariable("status")TaskStatusEnum status){
        return this.taskService.getAllTaskByTaskStatus(status);
    }

    @PatchMapping("mark_as_finished/{id}")
    public ResponseEntity<Void> maskTaskAsFinished(@PathVariable("id") Long id){
        this.taskService.markTaskAsFinished(id);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteTask(@PathVariable("id") Long id){
        this.taskService.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}
