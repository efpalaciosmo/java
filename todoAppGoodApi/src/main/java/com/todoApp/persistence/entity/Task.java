package com.todoApp.persistence.entity;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "task")
@NoArgsConstructor
@Getter
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false)
    private Long id;
    private String title;
    private String description;
    private LocalDateTime createdDate;
    private LocalDateTime eta; // finished estimated date
    private boolean finished;
    private TaskStatusEnum taskStatus;
    public Task(String title, String description, LocalDateTime createdDate, LocalDateTime eta, boolean finished, TaskStatusEnum status) {
        this.title = title;
        this.description = description;
        this.createdDate = createdDate;
        this.eta = eta;
        this.finished = finished;
        this.taskStatus = status;
    }
}
