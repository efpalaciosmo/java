package com.todoApp.mapper;

import com.todoApp.persistence.entity.Task;
import com.todoApp.persistence.entity.TaskStatusEnum;
import com.todoApp.service.DTOs.TaskInDto;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component // with this, TAskInDtoToTask can be injected
public class TaskInDtoToTask implements IMapper<TaskInDto, Task> {
    @Override
    public Task map(TaskInDto in) {
        Task task = new Task(in.getTitle(), in.getDescription(), LocalDateTime.now(), in.getEta(), false, TaskStatusEnum.ON_TIME);
        return task;
    }
}
