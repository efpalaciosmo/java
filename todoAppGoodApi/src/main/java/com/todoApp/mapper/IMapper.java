package com.todoApp.mapper;

public interface IMapper <Input, Output> {
    public Output map(Input in);
}
