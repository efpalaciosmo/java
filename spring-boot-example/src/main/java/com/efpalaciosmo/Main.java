package com.efpalaciosmo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@SpringBootApplication // To tell that this os a spring boot application
@RestController
@RequestMapping("/api/v2/customers")
public class Main {

    private final CustomerRepository customerRepository;

    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }
    public Main(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;

        Customer customer = new Customer(
                "Efrain",
                "efrla@gmail.com",
                24
        );
        customerRepository.save(customer);
    }
    @GetMapping()
    public List<Customer> getCustomers(){
        return customerRepository.findAll();
    }

    record NewCustomerRequest(
            String name,
            String email,
            Integer age
    ){}
    @PostMapping
    public void addCustomer(@RequestBody NewCustomerRequest request){
        Customer customer = new Customer(
                request.name,
                request.email,
                request.age
        );
        customerRepository.save(customer);
    }

    @DeleteMapping("{customerId}")
    public ResponseEntity<String> deleteCustomer(@PathVariable("customerId") Long id){
        if(!customerRepository.existsById(id)){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Customer not found");
        }
        String customerName = customerRepository.findById(id).get().getName();
        customerRepository.deleteById(id);
        return ResponseEntity.status(HttpStatus.ACCEPTED).body("Customer " + customerName + " deleted");
    }
    @PutMapping("{customerId}")
    public ResponseEntity<String> updateCustomer(@RequestBody NewCustomerRequest request, @PathVariable("customerId") Long id){
        if(!customerRepository.existsById(id)){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Customer not found");
        }
        Customer customerToUpdate = customerRepository.findById(id).get();
        customerToUpdate.setName(request.name);
        customerToUpdate.setEmail(request.email);
        customerToUpdate.setAge(request.age);
        customerRepository.save(customerToUpdate);
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(customerToUpdate.toString());
    }
}

